package main.ui;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import javax.swing.JButton;

import BlobUI.BlobPublishingCameraPanel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;

public class UserFrame extends JFrame {
	BlobUI.BlobPublishingCameraPanel cameraPanel = null;

	/**
	 * Create the frame.
	 */
	public UserFrame() {
		setTitle("Video Object Motion Tracking");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 600);
		getContentPane().setLayout(null);

		JPanel controlPanel = new JPanel();
		controlPanel.setBounds(10, 11, 414, 24);
		getContentPane().add(controlPanel);
		controlPanel.setLayout(null);

		final JButton btnClose = new JButton("Stop");
		final JButton btnOpenFile = new JButton("Open..");
		final JButton btnWebcam = new JButton("Webcam");

		btnClose.setVisible(false);
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (cameraPanel != null) {
					cameraPanel.setVisible(false);
					cameraPanel.closeCameraPanel();
					getContentPane().remove(cameraPanel);
					cameraPanel = null;
					btnOpenFile.setVisible(true);
					btnWebcam.setVisible(true);
					btnClose.setVisible(false);
				}
			}
		});

		btnOpenFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fc = new JFileChooser();
				String path = "";
				int returnVal = 0;
				JFileChooser jFileChooser1 = new javax.swing.JFileChooser();
				returnVal = jFileChooser1.showOpenDialog(UserFrame.this);
				if (returnVal == javax.swing.JFileChooser.APPROVE_OPTION) {
					try {
						path = jFileChooser1.getSelectedFile()
								.getCanonicalPath();
					} catch (IOException e) {
						JOptionPane.showMessageDialog(null,
								"Unable to open video");
					}
					cameraPanel = new BlobPublishingCameraPanel(path);
					if (cameraPanel.isProcessing() == false) {
						cameraPanel = null;
						btnOpenFile.setVisible(true);
						btnWebcam.setVisible(true);
						btnClose.setVisible(false);
					} else {
						cameraPanel.setVisible(true);
						cameraPanel.switchFeed(0);
						getContentPane().add(cameraPanel);
						btnOpenFile.setVisible(false);
						btnWebcam.setVisible(false);
						btnClose.setVisible(true);
					}
				}
			}
		});

		btnOpenFile.setBounds(0, 0, 89, 23);
		controlPanel.add(btnOpenFile);
		btnClose.setBounds(89, 0, 89, 23);
		controlPanel.add(btnClose);

		btnWebcam.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cameraPanel = new BlobPublishingCameraPanel("webcam");
				if (cameraPanel.isProcessing() == false) {
					cameraPanel = null;
					btnOpenFile.setVisible(true);
					btnWebcam.setVisible(true);
					btnClose.setVisible(false);
				} else {
					cameraPanel.setVisible(true);
					cameraPanel.switchFeed(0);
					getContentPane().add(cameraPanel);
					btnWebcam.setVisible(false);
					btnOpenFile.setVisible(false);
					btnClose.setVisible(true);
				}
			}
		});
		btnWebcam.setBounds(178, 0, 89, 23);
		controlPanel.add(btnWebcam);
	}
}
