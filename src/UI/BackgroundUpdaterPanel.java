
package UI;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import VideoProcessing.*;

public class BackgroundUpdaterPanel extends JPanel implements ActionListener {
	private BackgroundUpdater effect;
	private JButton btnReset;

	public BackgroundUpdaterPanel(BackgroundUpdater effect) {
		this.effect = effect;
		
		setLayout(new BorderLayout());
		btnReset = new JButton("Snapshot of Background");
		add(btnReset, BorderLayout.NORTH);
		btnReset.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		effect.resetBackground();
	}
}
