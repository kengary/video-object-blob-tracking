
package UI;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import VideoProcessing.*;

public class PixelizationDialate8BitPanel extends JPanel implements ChangeListener {
	private PixelizationDialate8Bit effect;
	private JSlider sldPixelSize;
	private JLabel lblThreshold;
	
	public PixelizationDialate8BitPanel(PixelizationDialate8Bit effect) {
		this.effect = effect;
		setLayout(new BorderLayout());
		sldPixelSize = new JSlider(1, 16);
		add(sldPixelSize, BorderLayout.CENTER);
		sldPixelSize.addChangeListener(this);
		lblThreshold = new JLabel();
		
		update();
	}

	public void update() {
		sldPixelSize.setValue(effect.getPixelSize());
		lblThreshold.setText(Integer.toString(effect.getPixelSize()));
	}
	
	@Override
	public void stateChanged(ChangeEvent e) {
		if(e.getSource() == sldPixelSize) {
			effect.setPixelSize(sldPixelSize.getValue());
			update();
		}
	}

}
