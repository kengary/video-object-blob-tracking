
package UI;

import javax.swing.JPanel;

import Util.ClassMappingFactory;
import VideoProcessing.*;
/**
 * Singleton that creates JPanel sub-classes for {@link RgbVideoEffect} sub-classes.
 * @author Joel R. Becker
 * <br>8/7/09
 */
public class VideoEffectUIFactory extends ClassMappingFactory<RgbVideoEffect, JPanel> {
	
	/** The singleton instance. */
	private static final VideoEffectUIFactory INSTANCE = new VideoEffectUIFactory();
	
	/** Constructor. */
	private VideoEffectUIFactory() {
		//// Register the ones we know of
		registerClass(RgbThresholdEffect.class, RgbThresholdPanel.class);
		registerClass(BackgroundUpdater.class, BackgroundUpdaterPanel.class);
		registerClass(PixelizationDialate8Bit.class, PixelizationDialate8BitPanel.class);
	}
	
	/**
	 * Returns the singleton instance.
	 * @return The singleton instance.
	 */
	public static VideoEffectUIFactory getInstance() {
		return INSTANCE;
	}
}
