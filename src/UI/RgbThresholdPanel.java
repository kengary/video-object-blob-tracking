
package UI;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import VideoProcessing.*;

/**
 * JPanel for {@link RgbThresholdEffect}.
 * @author Joel R. Becker
 *
 */
public class RgbThresholdPanel extends JPanel implements ChangeListener {
	private RgbThresholdEffect effect;
	private JSlider sldThreshold;
	private JLabel lblThreshold;
	
	public RgbThresholdPanel(RgbThresholdEffect effect) {
		this.effect = effect;
		setLayout(new BorderLayout());
		sldThreshold = new JSlider(0, 255);
		add(sldThreshold, BorderLayout.CENTER);
		sldThreshold.addChangeListener(this);
		lblThreshold = new JLabel();
		
		update();
	}

	public void update() {
		sldThreshold.setValue(effect.getThreshold());
		lblThreshold.setText(Integer.toString(effect.getThreshold()));
	}
	
	@Override
	public void stateChanged(ChangeEvent e) {
		if(e.getSource() == sldThreshold) {
			effect.setThreshold((char) sldThreshold.getValue());
			update();
		}
	}
}
