package VideoProcessing;

import java.awt.image.WritableRaster;

import javax.media.format.VideoFormat;

public class GrayScal extends RgbVideoEffect {

	@Override
	protected boolean processRGB(byte[] bin, byte[] bout, VideoFormat format) {
		int p = 0;
		for (int i = 0; i < bin.length; i += 3, p++) {
			int r, g, b;
			r =  byte2Int(bin[i + 2]);
			g =  byte2Int(bin[i + 1]);
			b =  byte2Int(bin[i]);
			int y = (int) (r * 0.3 + g * 0.59 + g * 0.11);
			bout[p] = (byte) y;
		}
		return true;
	}


	@Override
	public String getName() {
		return "Gray Scal Effect";
	}

	protected void updateImage(byte[] bout, VideoFormat vformat) {
		synchronized (displayImage) {
			// // Copy pixels to image
			WritableRaster rast = displayImage.getRaster();
			int[] pixel = new int[] { 0, 0, 0, 255 };
			int p = 0;
			for (int y = vformat.getSize().height - 1; y >= 0; y--) {
				for (int x = 0; x < vformat.getSize().width; x++) {
					pixel[0] = pixel[1] = pixel[2] = bout[p++];
					rast.setPixel(x, y, pixel);
				}
			}
		}
	}
}
