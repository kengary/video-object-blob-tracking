package VideoProcessing;

import java.awt.image.WritableRaster;

import javax.media.format.VideoFormat;

public class RgbThresholdEffect extends RgbVideoEffect {

	// private char threshold[] = new char[] {64, 64, 64};
	protected int threshold = 32;

	/** Number of pixels that passed the threshold. */
	protected int passCount;

	/** Ratio of pixels passing to pixels not passing. */
	protected float passRatio;

	public RgbThresholdEffect() {
	}

	public String getName() {
		return "RGB Threshold";
	}

	public void setThreshold(char threshold) {
		this.threshold = threshold;
	}

	public int getThreshold() {
		return threshold;
	}

	/**
	 * If either the R, G, or B components of a pixel pass the threshold, then
	 * the corresponding byte in the output buffer is set to 100% (255). NOTE:
	 * Output is a byte-per-pixel format, NOT 3 bytes per pixel.
	 */
	protected boolean processRGB(byte[] bin, byte[] bout, VideoFormat format) {
		/*
		 * if(supportedOuts == null) { supportedOuts = new Format[] { new
		 * IndexedColorFormat(format.getSize(), format.getMaxDataLength(),
		 * byte[].class, format.getFrameRate(), format.getSize().width, 8,
		 * LabelColors.REDS, LabelColors.GREENS, LabelColors.BLUES) }; }
		 */
		/*
		 * passCount = 0; int p = 0; // TODO bin.length / 3 ?? for (int i = 0; i
		 * < bin.length; i += 3, p++) { if ((char) bin[i] > threshold && (char)
		 * bin[i + 1] > threshold && (char) bin[i + 2] > threshold) { // 255 is
		 * actually -1 here bout[p] = (byte) 255; passCount++; } else { bout[p]
		 * = 0; } } passRatio = (float) passCount / (float)
		 * (format.getSize().width * format.getSize().height);
		 */
		// System.out.println(String.format("Threshold pass: %.2f",
		// passRatio*100));

		for (int i = 0; i < bin.length; i++) {
			if (byte2Int(bin[i]) > threshold) {
				bout[i] = 1;
			} else {
				bout[i] = 0;
			}
		}
		return true;
	}

	/** Returns the number of pixels that passed in the last frame. */
	public int getPassCount() {
		return passCount;
	}

	/**
	 * Returns the ratio of pixels passing to pixels not passing, in the last
	 * frame.
	 */
	public float getPassRatio() {
		return passRatio;
	}

	/** {@inheritDoc} */
	protected void updateImage(byte[] bout, VideoFormat vformat) {
		synchronized (displayImage) {
			// // Copy pixels to image
			WritableRaster rast = displayImage.getRaster();
			int[] pixel = new int[] { 0, 0, 0, 255 };
			int p = 0;
			for (int y = vformat.getSize().height - 1; y >= 0; y--) {
				for (int x = 0; x < vformat.getSize().width; x++) {
					pixel[0] = pixel[1] = pixel[2] = bout[p++];
					rast.setPixel(x, y, pixel);
				}
			}
		}
	}
}
