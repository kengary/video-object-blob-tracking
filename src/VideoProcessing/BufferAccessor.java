package VideoProcessing;

import java.awt.Dimension;
import java.awt.image.WritableRaster;

import javax.media.Format;
import javax.media.format.RGBFormat;
import javax.media.format.VideoFormat;

public class BufferAccessor extends RgbVideoEffect {

	private byte[] buffer;
	private Dimension size;
	private BufferAccessor previous = null;

	public BufferAccessor() {
	}

	public BufferAccessor(BufferAccessor previous) {
		this();
		this.previous = previous;
	}

	public byte[] getBuffer() {
		return this.buffer;
	}

	public Dimension getBufferSize() {
		return size;
	}

	public String getName() {
		return "Video Buffer Accessor";
	}

	protected boolean processRGB(byte[] bin, byte[] bout, VideoFormat format) {
		size = format.getSize();
		if (this.buffer == null) {
			this.buffer = new byte[bin.length];
		}
		if (this.previous == null || previous.getBuffer() == null) {
			System.arraycopy(bin, 0, this.buffer, 0, bin.length);
		} else if (previous.getBuffer() != null) {
			System.arraycopy(previous.getBuffer(), 0, this.buffer, 0,
					previous.getBuffer().length);
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		if (this.buffer == null) {
			hash = -1;
		} else {
			for (int i = 0; i < this.buffer.length; i++) {
				hash = (hash += (int) this.buffer[i] % 49999) % 49999;
			}
		}
		return hash;
	}


}
