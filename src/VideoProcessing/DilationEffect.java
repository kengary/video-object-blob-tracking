package VideoProcessing;

import java.awt.Dimension;

import javax.media.format.VideoFormat;

public class DilationEffect extends RgbVideoEffect {
	private static final int[] OX = { 0, -1, 0, 1, 1, 1, 0, -1, -1, -2, 2, 0,
			0, -1, 1, 2, 2, 1, -1, -2, -2, -2, 2, 2, -2, 0, 0, 1, 1, -1, -1 };
	private static final int[] OY = { 0, -1, -1, -1, 0, 1, 1, 1, 0, 0, 0, -2,
			2, -2, -2, -1, 1, 2, 2, -1, 1, -2, -2, 2, 2, 3, -3, 3, -3, 3, -3 };
	private static final int N = OX.length;
	private Dimension size;

	@Override
	protected boolean processRGB(byte[] bin, byte[] bout, VideoFormat format) {
		size = format.getSize();
		for (int y = 0; y < size.height; y++) {
			for (int x = 0; x < size.width; x++) {
				boolean dilate = false;
				for (int k = 0; k < N; k++) {
					int tx, ty;
					tx = x + OX[k];
					ty = y + OY[k];
					if (inBoundry(tx, ty, size.width, size.width)
							&& bin[(ty * size.width + tx)] == 1) {
						dilate = true;
						break;
					}
				}
				if (dilate) {
					bout[(y * size.width + x)] = 1;
				} else {
					bout[(y * size.width + x)] = 0;
				}
			}
		}
		return true;
	}

	private boolean inBoundry(int tx, int ty, int width, int width2) {
		return (0 <= tx && tx < size.width) && (0 <= ty && ty < size.height);
	}

	@Override
	public String getName() {
		return "Dilation";
	}

}
