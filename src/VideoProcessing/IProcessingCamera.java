
package VideoProcessing;

import javax.media.MediaLocator;



public interface IProcessingCamera {

	/**
	 * Opens the video stream at the given location.
	 * @return true if successful.
	 */
	boolean open(MediaLocator ml);

	/**
	 * Returns true if the camera is open.
	 * @return true if the camera is open.
	 */
	boolean isOpen();

	/**
	 * Closes the video input to the processing chain.
	 */
	void close();

	/**
	 * Gets the video processing chain.
	 * @return An array of {@link RgbVideoEffect}s.
	 */
	RgbVideoEffect[] getProcessingChain();

	/**
	 * Returns the first {@link RgbVideoEffect} in the chain that is an instance of the given class, or null if no such effect is in the chain.
	 * @param rgbVideoEffectClass The class of the desired effect.
	 * @return The effect, or null if non-existent.
	 */
	RgbVideoEffect getEffect(Class<? extends RgbVideoEffect> rgbVideoEffectClass);

	//void startRecording();

}
