
package VideoProcessing;

import java.awt.image.BufferedImage;


public interface VideoFrameListener {
	void newVideoFrame(BufferedImage image);
}
