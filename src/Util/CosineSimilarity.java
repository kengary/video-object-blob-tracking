package Util;

import Jama.Matrix;

public class CosineSimilarity {

	public static double computeSimilarity(Matrix sourceDoc, Matrix targetDoc) {
		double dotProduct = sourceDoc.arrayTimes(targetDoc).norm1();
		double eucledianDist = sourceDoc.normF() * targetDoc.normF();
		return dotProduct / eucledianDist;
	}

	public static double computeSimilarity(int[] a, int[] b) {
		double[][] a1 = new double[1][a.length]; 
		double[][] a2 = new double[1][a.length];
		for (int i=0;i<a.length;i++) {
			a1[0][i] = a[i];
			a2[0][i] = b[i];
		}
		Matrix m1,m2;
		m1 = new Matrix(a1);
		m2 = new Matrix(a2);
		return computeSimilarity(m1, m2);
	}
}
