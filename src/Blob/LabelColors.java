
package Blob;

import java.awt.Color;

public class LabelColors {
	static public final byte[] REDS;
	static public final byte[] GREENS;
	static public final byte[] BLUES;
	static public final Color[] COLORS;
	
	static {
		REDS = new byte[256];
		GREENS = new byte[256];
		BLUES = new byte[256];
		COLORS = new Color[] {
				Color.black, 	// 0 == NOT A LABEL
				Color.red,		// 1
				Color.blue,		// 2
				Color.yellow,	// 3
				Color.green,	// 4
				Color.cyan,		// 5
				Color.magenta,	// 6
				Color.orange	// 7
		};
		int i;
		for (i = 0; i < COLORS.length; i++) {
			REDS[i] = (byte) COLORS[i].getRed();
			GREENS[i] = (byte) COLORS[i].getGreen();
			BLUES[i] = (byte) COLORS[i].getBlue();
		}
		for(; i < 256; i++) {
			REDS[i] = GREENS[i] = BLUES[i] = (byte) 255;
		}
	}
}
