package Blob;

import java.awt.Rectangle;
import java.awt.geom.Point2D;

public class Blob {
	public static final int NOT_CORRELATED = -1;
	public int label;
	public int frameLabel;
	public int frameNumber;
	public Rectangle bounds;
	public int pixelCount;
	public double timeCreated; // timestamp in seconds
	public double timeLastUpdated; // age in seconds
	public double timeMissing; // total time that the blob has been missing (0
								// if not missing)
	public Point2D.Double velocity; // pixels per second
	public int[] histogram;

	/**
	 * Constructor for a new Blob that is not correlated with a previous one.
	 * 
	 * @param label
	 * @param bounds
	 */
	public Blob(int frameLabel, Rectangle bounds) {
		this.label = NOT_CORRELATED;
		this.frameLabel = frameLabel;
		this.bounds = bounds;
		// this.pixelCount = pixelCount;
		this.velocity = new Point2D.Double(0.0, 0.0);
		this.timeCreated = (double) System.nanoTime() / 1000000000.0;
		this.timeLastUpdated = this.timeCreated;
		this.timeMissing = 0.0;
	}

	public Blob(int frameLabel, Rectangle bounds, byte[] image) {
		this(frameLabel, bounds);
		this.histogram = getHistogram(image);
	}

	private int[] getHistogram(byte[] image) {
		int[] hist = new int[256];
		for (int i = 0; i < image.length; i++) {
			hist[(int) image[i] & 0xff]++;
		}
		for (int i = 0; i < image.length; i++) {
			hist[i] = hist[i] / image.length;
		}
		return hist;
	}
}
