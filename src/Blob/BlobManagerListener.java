
package Blob;

/**
 * Listens for {@link BlobManager} events, such as when a new blob is detected,
 * or when a blob has been lost.
 * @author Joel R. Becker
 * <br>8/24/09
 */
public interface BlobManagerListener {
	
	/**
	 * Called when a new blob has been detected (and correlated/reached age of acceptance).
	 * @param blob The new blob that was detected.
	 */
	void newBlobDetected(Blob blob);
	
	/**
	 * Called when a blob has been lost (and has reached the maximum missing time).
	 * @param blob The blob that was lost.
	 */
	void blobLost(Blob blob);
	
	/**
	 * Called when the blobs have been updated (via updateBlobs()).
	 */
	void blobsUpdated();
}
