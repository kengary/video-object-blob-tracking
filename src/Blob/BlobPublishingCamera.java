package Blob;

import VideoProcessing.*;

/**
 * Panel that streams video from a given source through a video processing
 * chain.
 * 
 * TODO Use noise-based threshold (higher threshold for noisier pixels)
 */
public class BlobPublishingCamera extends AbstractProcessingCamera {

	protected BlobManager blobManager;

	
	
	public BlobPublishingCamera() {
		this.blobManager = new BlobManager();
	}
	
	protected RgbVideoEffect[] createEffectChain() {
		BufferAccessor bufferAccr = new BufferAccessor();
		BackgroundUpdater background = new BackgroundUpdater(bufferAccr);
		return new RgbVideoEffect[] {
				new BufferAccessor(),
				new GrayScal(),
				new Smoothing(),
				new LowerResolution(),
				bufferAccr,
				new RgbDiffEffect(background),
				new RgbThresholdEffect(),
				new PixelizationDialate8Bit(),
				background,
				//new DilationEffect(),
				//new ErosionEffect(),
				//new LabelEffect(),
				new FourNeighborBlobDetector(bufferAccr, blobManager, 5, 256, 640*480),
		};
	}
	
/*	protected RgbVideoEffect[] createEffectChain() {
		BufferAccessor firstPre, secondPre,current;
		current = new BufferAccessor();
		firstPre = new BufferAccessor(current);
		secondPre = new BufferAccessor(firstPre);
		return new RgbVideoEffect[] {
				new BufferAccessor(),
				new GrayScal(),
				new Smoothing(),
				new LowerResolution(),
				secondPre,
				firstPre,
				current,
				new Diffeffect(firstPre,secondPre),
				new PixelizationDialate8Bit(),
				//new DilationEffect(),
				//new ErosionEffect(),
				//new LabelEffect(),
				new FourNeighborBlobDetector(blobManager, 5, 256, 640*480),
		};
	}*/

	/**
	 * Gets the {@link BlobManager} for this camera.
	 * @return the {@link BlobManager} for this camera.
	 */
	public BlobManager getBlobManager() {
		return blobManager;
	}
	
}
