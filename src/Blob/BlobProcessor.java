
package Blob;

import java.util.List;


/**
 * Interface for an object that processes a list of blobs.
 * @author Joel R. Becker
 * <br>8/10/09
 */
public interface BlobProcessor {
	/**
	 * Processes a list of blobs, possibly adding to and removing from the list.
	 * @param input The input list of {@link Blob}s.
	 * @return The processed list; may be the same list given as input, but modified.
	 */
	List<Blob> process(List<Blob> input);
}
