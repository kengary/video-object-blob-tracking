
package Blob;

import java.util.List;



/**
 * <p>Correlates new blobs with those from a previous video frame. That is, it it
 * essentially creates persistent (multi-frame) blobs from uncorrelated blobs,
 * which only last one frame.</p>
 * 
 * <p>The resulting blobs have a different set of labels completely. While the
 * per-frame blobs are labeled from 1-n, where n is the number of blobs in that
 * frame, a correlated blob is labeled arbitrarily, as well as uniquely from any
 * previous correlated blobs.</p>
 *  
 * @author Joel R. Becker
 * <br>8/10/09
 */
public interface BlobCorrelator {
	/**
	 * Correlates the new blobs with the previous blobs.
	 * @param newBlobs The new Blobs
	 * @param previousBlobs The previous Blobs to correlate the new ones with.
	 * @return The correlated list of Blobs.
	 */
	List<Blob> correlate(List<Blob> newBlobs, List<Blob> previousBlobs);
}
