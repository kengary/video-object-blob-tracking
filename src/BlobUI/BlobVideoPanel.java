
package BlobUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.List;

import Blob.Blob;
import Blob.BlobPublishingCamera;
import UI.VideoPanel;



public class BlobVideoPanel extends VideoPanel {

	private static final Color DEFAULT_UNCORRELATED_BLOB_COLOR = Color.GREEN;

	private static final Color DEFAULT_BLOB_COLOR = Color.RED;

	private static final Color DEFAULT_HIDDEN_BLOB_COLOR = Color.YELLOW;
	
	private static final Font DEFAULT_BLOB_LABEL_FONT = new Font(Font.MONOSPACED, Font.PLAIN, 10); 

	/** The source {@link BlobPublishingCamera}. */
	private BlobPublishingCamera camera;
	
	private Color uncorrelatedBlobColor = DEFAULT_UNCORRELATED_BLOB_COLOR;
	
	private Color blobColor = DEFAULT_BLOB_COLOR;
	
	private Color hiddenBlobColor = DEFAULT_HIDDEN_BLOB_COLOR;
	
	private Font blobLabelFont = DEFAULT_BLOB_LABEL_FONT;
	
	private boolean drawBlobs = true;
	
	
	
	public BlobVideoPanel(BlobPublishingCamera camera) {
		if (camera == null) {
			throw new NullPointerException();
		}
		this.camera = camera;
	}
	/*
	private Rectangle2D.Double toImageCoord(Rectangle2D.Double)) {
		
	}*/
	
	public void setDrawBlobs(boolean drawBlobs) {
		this.drawBlobs = drawBlobs;
	}
	

	/**
	 * Overridden to draw blob rectangles and labels into video frame before displaying.
	 */
	@Override
	public void newVideoFrame(BufferedImage image) {
		
		if (drawBlobs) {
			Graphics2D gr = (Graphics2D) image.getGraphics();
			
			gr.setFont(blobLabelFont);
			
			//// Draw uncorrelated blobs
			/*List<Blob> ublobs = camera.getBlobManager().getUncorrelatedBlobList();
			gr.setColor(uncorrelatedBlobColor);
			gr.setPaint(uncorrelatedBlobColor);
			for (Blob b : ublobs) {
				Rectangle bounds = b.bounds;
				gr.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
				gr.drawString(Integer.toString(b.frameLabel), bounds.x, bounds.y + 10);
			}*/
			
			//// Draw correlated (possibly hidden) blobs
			List<Blob> cblobs = camera.getBlobManager().getBlobList();
			for (Blob b : cblobs) {
				Rectangle bounds = b.bounds;
				if (b.timeMissing > 0.0) {
					gr.setColor(hiddenBlobColor);
					//gr.setPaint(uncorrelatedBlobColor);
				} else {
					gr.setColor(blobColor);
					//gr.setPaint(uncorrelatedBlobColor);
				}
				gr.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
				gr.drawString(Integer.toString(b.label), bounds.x, bounds.y - 1);
			}
		}
		
	
		
		super.newVideoFrame(image);
	}

	private void unionIntersectedBlobs(List<Blob> blobs) {
		while (checkIntersection(blobs)) {
			for (int i = 0; i < blobs.size(); i++) {
				for (int j = i + 1; j < blobs.size(); j++) {
					Rectangle a, b;
					a = blobs.get(i).bounds;
					b = blobs.get(j).bounds;
					if (a.intersects(b)) {
						a = a.union(b);
						blobs.get(i).bounds = a;
						blobs.remove(j);
						j--;
					}
				}
			}
		}
	}

	private boolean checkIntersection(List<Blob> blobs) {
		int len = blobs.size();
		boolean inter = false;
		for (int i = 0; i < len; i++) {
			for (int j = i + 1; j < len; j++) {
				Rectangle a, b;
				a = blobs.get(i).bounds;
				b = blobs.get(j).bounds;
				if (a.intersects(b)) {
					inter = true;
					break;
				}
			}
		}
		return inter;
	}
}
