
package BlobUI;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import Blob.FourNeighborBlobDetector;


public class FourNeighborBlobDetectorPanel extends JPanel {
	
	private FourNeighborBlobDetector effect;
	private JTextField txtMinBlobSize;
	
	
	public FourNeighborBlobDetectorPanel(FourNeighborBlobDetector effect) {
		this.effect = effect;
		
		GridLayout gridLayout = new GridLayout(0, 1);
		setLayout(gridLayout);
		
		JPanel pnl = new JPanel();
		pnl.setBorder(new TitledBorder(new EtchedBorder(), "Min. Blob Size"));
		pnl.setLayout(new BorderLayout());
		txtMinBlobSize = new JTextField(14);
		pnl.add("Center", txtMinBlobSize);
		add(pnl);
		txtMinBlobSize.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent event) {
				try {
					FourNeighborBlobDetectorPanel.this.effect.setMinBlobSize(Integer.parseInt(
							((JTextField) (event.getSource())).getText()));
				} catch (NumberFormatException e) {
					// do nothing
				}
				super.focusLost(event);
			}
		});
		

		update();
	}
	
	public void update() {
		txtMinBlobSize.setText(Integer.toString(effect.getMinBlobSize()));
		
	}
}
